from tkinter import *
from random import *


stolpci = 40            # število stolpcev v matriki
vrstice = 40            # število vrstic v matriki
debelina = 20           # debelina kače
zacetnadolzina = 4      # začetna dolžina kače
x1, y1 = randint(5,stolpci-7) , randint(5,vrstice-5)    # izbira začetno pozicijo kače
x2, y2 = randint(5,stolpci-7) , randint(5,vrstice-5)

class kaca():
    def __init__(self,master):
        self.platno = Canvas(master, width = stolpci*debelina, height = vrstice*debelina,bg = "white")
        self.platno.pack()

        menu = Menu(master)
        master.config(menu=menu)
        
        igra_menu = Menu(menu)
        menu.add_cascade(label="Igra", menu=igra_menu)
        igra_menu.add_command(label="Nova igra    (r)", command=self.nova_igra)
        igra_menu.add_separator()
        igra_menu.add_command(label="Izhod           (q)", command=self.platno.master.destroy)

        tezavnost_menu = Menu(menu)
        menu.add_cascade(label="Težavnost", menu=tezavnost_menu)
        tezavnost_menu.add_command(label="Lahko", command=self.lahka_tezavnost)
        tezavnost_menu.add_command(label="Težko   (Lee-jev algoritem)", command=self.tezka_tezavnost)
        
        root.bind("<Key>", self.upravljanje)  # samo za start, restart in exit
        root.bind("<Left>", self.obrnilevo)
        root.bind("<Up>", self.obrnigor)
        root.bind("<Down>", self.obrnidol)
        root.bind("<Right>", self.obrnidesno)

        # kača oseba
        self.koordinate = []        # shranjevanje koordinat kače
        self.smer = (0,-1)          # začetna smer
        self.maks = zacetnadolzina  # začetna dolžina kače, tudi števec za maksimalno vrednost


        # kača računalnik
        self.koordinate2 = []
        self.smer2 = (1,0)

        self.tezavnost = 0
        self.celice =  []
        self.hrana_x = None
        self.hrana_y = None
        self.hrana = False
        self.text_id = None

        self.zacetnamatrika()   # ustvari začetno pozicijo
        self.narisi()           # nariše samo začetno kačo
        self.konec = False

    def lahka_tezavnost(self):
        self.tezavnost = 0
        
    def tezka_tezavnost(self):
        self.tezavnost = 1
        
    def zacetnamatrika(self):
        # ustvari začetno pozicijo kače
        x1, y1 = randint(5,stolpci-7) , randint(5,vrstice-5)
        x2, y2 = randint(5,stolpci-7) , randint(5,vrstice-5)
        self.matrika = [ [ 0 for i in range(vrstice) ] for j in range(stolpci) ]
        for i in range(zacetnadolzina+1):
            self.matrika[x1][y1+i] = i+1
            self.matrika[x2+i][y2] = i+1
            self.koordinate.append((x1,y1+i))
            self.koordinate2.append((x2+i,y2))
        self.text_id = self.platno.create_text(stolpci*debelina/2,vrstice*debelina/2,font=("Purisa",24),fill="green",text = "Izberi težavnost v meniju.\nPritisni S za začetek. Tvoja kača je črne barve.")
                
    def narisi(self):
        # nariše samo začetno kačo človek
        for i in self.koordinate:
            x0 = i[0]*debelina
            x = (i[0]+1)*debelina
            y0 = i[1]*debelina
            y = (i[1]+1)*debelina
            self.platno.create_oval(x0,y0,x,y,fill="black")
            
        # nariše samo začetno kačo računalnik
        for i in self.koordinate2:
            x0 = i[0]*debelina
            x = (i[0]+1)*debelina
            y0 = i[1]*debelina
            y = (i[1]+1)*debelina
            self.platno.create_oval(x0,y0,x,y,fill="blue")

                    
    def ponovnorisi(self):
        if not self.konec and self.tezavnost==0:
            self.smer2 = self.logicna_smer()
            self.premik()
            self.premik2()
            self.narisipoeno()
            self.platno.after(100, self.ponovnorisi)
            if self.maks % 60 == 0 and self.hrana==False:
                self.skrajsaj()
                
        elif not self.konec and self.tezavnost==1:
            self.smer2 = self.logicna_smer2()
            self.premik()
            self.premik2()
            self.narisipoeno()
            self.platno.after(160, self.ponovnorisi)
            if self.maks % 60 == 0 and self.hrana==False:
                self.skrajsaj()
            


    def narisipoeno(self):
        # kača človek
        x,y,nekaj = self.najdiglavo()
        a = self.koordinate[-2][0]
        b = self.koordinate[-2][1]
        celica = self.platno.create_oval(x*debelina,y*debelina,(x+1)*debelina,(y+1)*debelina,fill="red")
        #self.celice.append*celica(
        if self.konec == False:
            self.platno.create_oval(a*debelina,b*debelina,(a+1)*debelina,(b+1)*debelina,fill="black")

        # kača računalnik
        x2,y2,nekaj = self.najdi_glavo2()
        c = self.koordinate2[-2][0]
        d = self.koordinate2[-2][1]
        self.platno.create_oval(x2*debelina,y2*debelina,(x2+1)*debelina,(y2+1)*debelina,fill="purple")
        if self.konec == False:
            self.platno.create_oval(c*debelina,d*debelina,(c+1)*debelina,(d+1)*debelina,fill="blue")


    def premik(self):
        x,y,self.maks = self.najdiglavo()
        if y == vrstice-1 and self.smer == (0,-1):      # spodnji rob prehoden
            self.matrika[x][0] = self.maks+1
            self.koordinate.append((x,0))            
        elif x == stolpci-1 and self.smer==(1,0):       # desni rob prehoden
            self.matrika[0][y] = self.maks+1
            self.koordinate.append((0,y))
        elif x ==0 and self.smer==(-1,0):               # levi rob prehoden
            self.matrika[stolpci-1][y] = self.maks+1
            self.koordinate.append((stolpci-1,y))
        elif y==0 and self.smer==(0,1):                 # zgornji rob prehoden
            self.matrika[x][vrstice-1] = self.maks+1
            self.koordinate.append((x,vrstice-1))
        elif self.matrika[x+self.smer[0]][y-self.smer[1]] == -1:        # pobiranje elementa, ki skrajsa kačo
            a,b,nekaj = self.najdi_glavo2()
            self.platno.delete(ALL)
            self.matrika = [ [ 0 for i in range(vrstice) ] for j in range(stolpci) ]
            
            self.matrika[x+self.smer[0]][y-self.smer[1]] = 2
            self.matrika[x][y] = 1
            self.koordinate = [(x,y),(x+self.smer[0],y-self.smer[1])]

            self.matrika[a+self.smer2[0]][b-self.smer2[1]] = 2
            self.matrika[a][b] = 1
            self.koordinate2 = [(a,b),(a+self.smer2[0],b-self.smer2[1])]
            self.narisi()
            self.hrana = False
            
        elif self.matrika[x+self.smer[0]][y-self.smer[1]] > 0:          # preveri ali si se zaletel
            self.konec = True
            self.platno.create_text(stolpci*debelina/2,vrstice*debelina/2, font=("Purisa",24),fill="green",
                                    text = " KONEC IGRE! \n\n Naslednjič se bolj potrudi.")

            
        else:
            self.matrika[x+self.smer[0]][y-self.smer[1]] = self.maks + 1  # običajni premik
            self.koordinate.append((x+self.smer[0],y-self.smer[1]))


    def premik2(self):
        # premik kače računalnik
        x,y,maksimum = self.najdi_glavo2()
        if y == vrstice-1 and self.smer2 == (0,-1):       # spodnji rob prehoden
            self.matrika[x][0] = maksimum+1        
            self.koordinate2.append((x,0))
        elif x == stolpci-1 and self.smer2==(1,0):       # desni rob prehoden
            self.matrika[0][y] = maksimum+1
            self.koordinate2.append((0,y))
        elif x ==0 and self.smer2==(-1,0):               # levi rob prehoden
            self.matrika[stolpci-1][y] = maksimum+1
            self.koordinate2.append((stolpci-1,y))
        elif y==0 and self.smer2==(0,1):                 # zgornji rob prehoden
            self.matrika[x][vrstice-1] = maksimum+1
            self.koordinate2.append((x,vrstice-1))
        elif self.matrika[x+self.smer2[0]][y-self.smer2[1]] == -1:        # pobiranje elementa, ki skrajsa kačo
            a,b,nekaj = self.najdiglavo()
            self.platno.delete(ALL)
            self.matrika = [ [ 0 for i in range(vrstice) ] for j in range(stolpci) ]
            
            self.matrika[x+self.smer2[0]][y-self.smer2[1]] = 2
            self.matrika[x][y] = 1
            self.koordinate2 = [(x,y),(x+self.smer2[0],y-self.smer2[1])]
            
            self.matrika[a+self.smer[0]][b-self.smer[1]] = 2
            self.matrika[a][b] = 1
            self.koordinate = [(a,b),(a+self.smer[0],b-self.smer[1])]

            self.hrana = False
            
        elif self.matrika[x+self.smer2[0]][y-self.smer2[1]] > 0:          # preveri ali si se zaletel
            self.konec = True
            self.platno.create_text(stolpci*debelina/2,vrstice*debelina/2,font=("Purisa",24),fill="green",
                                    text = " KONEC IGRE! \n\n Zmagal si.")
        else:
            self.matrika[x+self.smer2[0]][y-self.smer2[1]] = maksimum + 1  # običajni premik
            self.koordinate2.append((x+self.smer2[0],y-self.smer2[1]))
        

        
    def najdiglavo(self):
        # poišče glavo pri kači človek
        x = self.koordinate[-1][0]
        y = self.koordinate[-1][1]
        maksimum = self.matrika[x][y]
        return (x,y,maksimum)

    def najdi_glavo2(self):
        # poišče glavo pri kači računalnik
        x = self.koordinate2[-1][0]
        y = self.koordinate2[-1][1]
        maksimum = self.matrika[x][y]
        return (x,y,maksimum)
        
      
    def upravljanje(self, event):
        if event.char == "s":                 # pritisni s za start
            self.platno.delete(self.text_id)
            self.ponovnorisi()
        elif event.char == "r":
            self.nova_igra()
        elif event.char == "q":
            self.platno.master.destroy()

            
    def nova_igra(self):
        self.platno.delete(ALL)
        self.koordinate = []
        self.koordinate2 = []
        self.zacetnamatrika()
        self.smer = (0,-1)
        self.smer2=(1,0)
        self.maks = zacetnadolzina
        self.narisi()
        self.konec = False
        self.hrana = False
        
            
    def obrnilevo(self,event):
        if self.smer == (0,1) or self.smer == (0,-1):
            self.smer = (-1,0)

    def obrnidesno(self,event):
        if self.smer == (0,1) or self.smer == (0,-1):
            self.smer = (1,0)

    def obrnigor(self,event):
        if self.smer == (1,0) or self.smer == (-1,0):
            self.smer = (0,1)

    def obrnidol(self,event):
        if self.smer == (1,0) or self.smer == (-1,0):
            self.smer = (0,-1)


    def skrajsaj(self):
        # Ustvari element, ki skrajša kačo
        i, j  = randint(0,stolpci-1) , randint(0,vrstice-1)
        if self.matrika[i][j] == 0:
            self.matrika[i][j] = -1
            self.platno.create_oval(i*debelina,j*debelina,(i+1)*debelina,(j+1)*debelina,fill="green")
            self.hrana_x = i
            self.hrana_y = j
            self.hrana = True


    def logicna_smer(self):
        # definiramo logično izbiranje smeri kače računalnik tako,
        # da se izmika oviri
        x,y,maksimum = self.najdi_glavo2()
        koordinati = (x+self.smer2[0],y-self.smer2[1])
        if koordinati in self.koordinate or koordinati in self.koordinate2:
            return self.zamenjaj_smer()
        else:
            return self.smer2
        
    def zamenjaj_smer(self):
        # zamenja smer, če je polje zasedeno, tako da izbere prosto smer. Če so na voljo 2 smeri izbere naključno
        navpicno = ((0,1),(0,-1))
        vodoravno = ((1,0),(-1,0))
        novasmer = navpicno if self.smer2 == (1,0) or self.smer2 == (-1,0) else vodoravno
        izbira = choice((0,1))
        zasedeno = novasmer[izbira]
        x,y,maksimum = self.najdi_glavo2()
        koordinati = (x+zasedeno[0],y-zasedeno[1])
        if koordinati in self.koordinate or koordinati in self.koordinate2:
            izbira = 1-izbira
        return novasmer[izbira]

    

                
    def logicna_smer2(self):
        # Algoritem kače računalnik, ki zamenja smer Lee-jevem algoritmu, če je hrana na polju.
        # Če hrane ni na polju uporabi logicna_smer(), ki se izmika oviri.
        # Algoritem ni povsem optimiziran, zato logicna_smer2() potrebuje več časa za razmislek.
        if not self.hrana:
            return self.logicna_smer()
        
        else:
            m = [ [ -1 for i in range(vrstice) ] for j in range(stolpci) ] # matrika v katero shranjujem dolzine poti
            m[self.hrana_x][self.hrana_y] = 0
            zePregledani = [(self.hrana_x,self.hrana_y)] # seznam že pregledanih polj
            i = 0            
            while True:
                if i>=len(zePregledani):  # če ni možno po metodi lee-ja priti do rešitve, se kača izmika oviri 
                    return self.logicna_smer()
                else:
                    x,y,maksimum = self.najdi_glavo2()
                    prosti = self.prosti_sosedi(zePregledani[i][0],zePregledani[i][1])
                    vrednost = m[zePregledani[i][0]][zePregledani[i][1]]
                    for j in prosti:
                        if j not in zePregledani:
                            zePregledani.append(j)         # v zePregledani dodam proste sosede
                            m[j[0]][j[1]] = vrednost + 1   # v matriki jim povečam vrednost za 1
                            
                            if j == (x,y):   # če najdemo glavo, potem izberemo tisto smer, ki vsebuje najkrajšo pot
                                sosedi = self.prosti_sosedi(x,y)
                                maksVrednost = vrstice*stolpci
                                skorajSmer = None
                                for a,b in sosedi:    # zanka nam pomaga najti pravo smer 
                                    if m[a][b] <= maksVrednost and m[a][b]!=-1:
                                        maksVrednost = m[a][b]
                                        skorajSmer = (a,b)
                                return (skorajSmer[0]-x,y-skorajSmer[1])
                i += 1

            
    def filtriraj(self,sosedi):
        # funkcija iz seznama koordinat odstrani tiste, ki so izven območja
        filtrirani = []
        for i in sosedi:
            if i[0] in range(vrstice) and i[1] in range(stolpci):
                filtrirani.append(i)
        return filtrirani
        

    def prosti_sosedi(self,i,j):
        # funkcija vrača vse proste sosede od koordinate (i,j)
        vsi = [(i-1,j),(i+1,j),(i,j+1),(i,j-1)]
        vsi = self.filtriraj(vsi)
        prosti = []
        for x,y in vsi:
            if (x,y)==self.koordinate2[-1]:
                return [(x,y)]
            elif (x,y) not in self.koordinate and (x,y) not in self.koordinate2:
                prosti.append((x,y))
        return prosti
                

        
root = Tk()
root.title("Achtung, die Kurve!")
aplikacija = kaca(root)
root.mainloop()
